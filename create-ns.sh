oc process -f project_template.yaml\
	-p PROJECT_NAME=diot-dev \
	-p PROJECT_DISPLAYNAME='IoT dev' \
	-p PROJECT_DESCRIPTION='Optional description' \
	-p PROJECT_ADMIN_GROUP='CSEGRP-BU-DAHL-MYNS-ADMINS' \
	-p PROJECT_REQUESTING_USER='f5267387' \
	-p ZONE=int \
	-p STAGE=nonprod | oc create -f -
