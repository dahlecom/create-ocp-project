#!/bin/bash

usage() { echo "Usage: $0 -n <namespace> -z {int|dmz}" 1>&2; exit 1; }

while getopts ":n:z:" o; do
    case "${o}" in
        n)
            namespace=${OPTARG}
            [ -z $namespace ] && usage
            ;;
        z)
            zone=${OPTARG}
            [ -z $zone ] && usage
            ;;
        *)
            usage
            ;;
    esac
done

case $zone in
	"int")
		subnet=1000
		;;
	"dmz")
		subnet=1004
		;;
	*)
		usage
		;;
esac

ip=$(curl -s -k -X GET --header 'Accept: application/json' --header 'Authorization: Basic foo' "https://cmdb.dahl.se/api/1.0/suggest_ip/?subnet_id=${subnet}&reserve_ip=yes&mask_bits=25" | jq -r '.ip')
curl -s -k -X POST --header 'Content-Type: application/x-www-form-urlencoded' --header 'Accept: application/json' --header 'Authorization: Basic foo' -d "ipaddress=${ip}&label=${namespace}%20egressIP" 'https://cmdb.dahl.se/api/1.0/ips/'

oc patch netnamespace ${namespace} --type=merge -p "{\"egressIPs\": [\"$ip\"]}"
